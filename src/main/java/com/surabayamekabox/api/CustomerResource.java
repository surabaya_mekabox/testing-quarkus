package com.surabayamekabox.api;

import com.surabayamekabox.model.Customer;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/customer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class CustomerResource {
    @Inject
    EntityManager em;

    @GET
    public Response getAll() {
        List<Customer> customerList = Customer.findAll().list();
        return Response.ok(customerList).build();
    }

    @POST
    @Transactional
    public Response create(Customer customer) {
        customer.persist();
        return Response.ok(customer).build();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public Response update(@PathParam String id, Customer customer) {
        Customer cOriginal = Customer.findById(Long.parseLong(id));
        cOriginal.customerName = customer.customerName;
        em.merge(cOriginal);
        return Response.ok(customer).build();
    }

    @GET
    @Path("{id}")
    public Response show(@PathParam Long id) {
        Customer customer = Customer.findById(id);
        return Response.ok(customer).build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam Long id) {
        Customer.deleteById(id);
        return Response.ok().build();
    }
}
