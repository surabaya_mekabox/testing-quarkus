package com.surabayamekabox.api;

import com.surabayamekabox.model.SalesOrder;

import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("sales_order")
public class SalesOrderResource {
    @POST
    @Transactional
    public Response create(SalesOrder salesOrder) {
        salesOrder.totalValue = salesOrder.quantity * salesOrder.unitPrice;
        salesOrder.persist();
        return Response.ok().build();
    }

    @GET
    public Response getAll() {
        List<SalesOrder> salesOrderList = SalesOrder.listAll();
        return Response.ok(salesOrderList).build();
    }
}
