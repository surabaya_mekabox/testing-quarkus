package com.surabayamekabox.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class Customer extends PanacheEntity {
    public String customerName;
    public String address;
    public String phoneNumber;
}
