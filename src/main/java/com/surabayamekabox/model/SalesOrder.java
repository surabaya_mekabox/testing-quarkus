package com.surabayamekabox.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
public class SalesOrder extends PanacheEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    public Customer customer;
    public String namaBarang;
    public Long quantity;
    public Long unitPrice;
    public Long totalValue;
}
